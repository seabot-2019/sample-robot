#pragma once
#include <thread>
#include <frc/DigitalInput.h>
#include <frc/PWMSpeedController.h>
#include "defines.h"


class Elevator
{
public:
    // Enums
    enum Position {BOTTOM, MIDDLE, TOP, UNKNOWN};
    enum Status {RUNNING, STOPPED};

    // Constructors
    Elevator ();
    Elevator (frc::PWMSpeedController   *pMotor,
              frc::DigitalInput         *pLowSwitch, 
              frc::DigitalInput         *pMidSwitch,
              frc::DigitalInput         *pHighSwitch);

    // Funtioncs
    void MoveTo(Position position);
    void MoveTo_Thread (Position position);

    void GoUp();
    void GoDown();
    void Stop();

    void StopThread();

    void Update();

private:
    frc::DigitalInput       *m_pLowerLimitSwitch;
    frc::DigitalInput       *m_pMidLimitSwitch;
    frc::DigitalInput       *m_pUpperLimitSwitch;

    frc::PWMSpeedController *m_pMotor;

    bool m_fStop;
    bool m_fThreadRunning;
    Position m_CurrentPosition;
    Position m_LastPostion;
    std::thread *m_pThread;

    float m_speed;
    Position m_destination;
    Status m_threadStatus;
    frc::DigitalInput *m_pDestinationSwitch;
};

