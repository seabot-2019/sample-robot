#include <Lights.h>

constexpr float Lights::patterns [PatternCount];

Lights::Lights(frc::Spark* spark) {
	m_spark = spark;
}

Lights::~Lights() {
	// TODO Auto-generated destructor stub
}

void Lights::SetPattern(Pattern pattern)
{
	m_spark->Set(patterns[pattern]);
}
