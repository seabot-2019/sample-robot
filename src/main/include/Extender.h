#pragma once
#include <thread>
#include "defines.h"
#include <frc/Relay.h>
#include <frc/Encoder.h>
#include <frc/DigitalInput.h>

class Extender
{
public:
    enum Action {EXTEND, RETRACT};
    enum Status {RUNNING, STOPPED};

    Extender ();
    Extender (frc::Relay *pRelay, frc::DigitalInput *pLimitSwitch);

    void Move (Action action, bool fUseEncoder = true);
    void Move_thread (Action action, bool fUseEncoder = true);
    Status GetThreadStatus();
    void StopThread();

    void Extend();
    void Retract();

private:
    frc::Relay *m_pRelay;
    // frc::Encoder *m_pEncoder;
    frc::DigitalInput *m_pLimitSwitch;
    std::thread *m_pThread;
    Status m_threadStatus;
    bool m_fStop;

};