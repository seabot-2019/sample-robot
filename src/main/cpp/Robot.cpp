/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"
#include "LineSensor.h"
#include "Lights.h"

#include <iostream>
#include <thread>
#include <chrono>

#include <frc/Timer.h>
#include <frc/smartdashboard/SmartDashboard.h>
#include <frc/DigitalInput.h>
#include <cameraserver/CameraServer.h>
#include <frc/shuffleboard/BuiltInWidgets.h>

void blinkLight(int limit)
{
  bool value = false;
  int count = 0;
  while (count < limit)
  {
    frc::SmartDashboard::PutBoolean ("DB/LED 0", !value);
    //printf ("value: %B", value);
    value = !value;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    count++;
  }
}

Robot::Robot() {
  // Note SmartDashboard is not initialized here, wait until RobotInit() to make
  // SmartDashboard calls
  m_robotDrive.SetExpiration(0.1);
}

void Robot::RobotInit() {
  m_chooser.SetDefaultOption(kAutoNameDefault, kAutoNameDefault);
  m_chooser.AddOption(kAutoNameCustom, kAutoNameCustom);
  frc::SmartDashboard::PutData("Auto Modes", &m_chooser);

  // Initialize Camera stuff
  this->m_camera = frc::CameraServer::GetInstance()->StartAutomaticCapture(0);
  this->m_cameraServer = frc::CameraServer::GetInstance()->GetServer();
  this->m_cameraServer.SetSource (this->m_camera);

  // Initialize Relays
  this->m_pExtenderRelay  = new frc::Relay(EXTENDER_RELAY, frc::Relay::kBothDirections);
  this->m_pGripperRelay   = new frc::Relay(GRIPPER_RELAY, frc::Relay::kBothDirections);

  // Initialize Encoders
  // this->m_pExtenderEncoder  = new frc::Encoder(EXTENDER_ENCODER_A_DIO, EXTENDER_ENCODER_B_DIO);
  // this->m_pGripperEncoder   = new frc::Encoder(GRIPPER_ENCODER_A_DIO, GRIPPER_ENCODER_B_DIO);
  this->m_pExtenderLimitSwitch = new frc::DigitalInput(EXTENDER_LIMITSWITCH_DIO);

  // Initialize Elevator Components
  this->m_pElevatorMotor        = new frc::Spark (ELEVATOR_MOTOR_PWM);
  this->m_pElevatorTopSwitch    = new frc::DigitalInput(ELEVATOR_TOP_DIO);
  this->m_pElevatorMiddleSwitch = new frc::DigitalInput(ELEVATOR_MIDDLE_DIO);
  this->m_pElevatorBottomSwitch = new frc::DigitalInput(ELEVATOR_BOTTOM_DIO);

  // Initialize Line Sensors
  this->m_pLeftLineSensor   = new frc::DigitalInput(LINE_SENSOR_LEFT_DIO);
  this->m_pCenterLineSensor = new frc::DigitalInput(LINE_SENSOR_CENTER_DIO);
  this->m_pRightLineSensor  = new frc::DigitalInput(LINE_SENSOR_RIGHT_DIO);
  this->m_pLineSensor       = new LineSensor(this->m_pLeftLineSensor,
                                             this->m_pCenterLineSensor,
                                             this->m_pRightLineSensor);

  // Intialize action flags
  this->m_autopilot = false;
  this->m_resetGyro = false;
 
  // Initialize Gyro
  this->m_pGyro = new frc::AnalogGyro (GYRO_AI);
  this->m_pGyro->SetSensitivity(frc::AnalogGyro::kDefaultVoltsPerDegreePerSecond);
  this->m_pGyro->Calibrate();
  this->m_pGyro->Reset();
  
  // Initialize Accelerometer
  this->m_pAccelerometer = new frc::AnalogAccelerometer(ACCELEROMETER_AI);
  this->m_pAccelerometer->SetSensitivity(ACCELEROMETER_SENSITIVITY);
  this->m_pAccelerometer->SetZero(ACCELEROMETER_ZERO);

  // Initialize Power Distribution Panel
  // this->m_pPDP = new frc::PowerDistributionPanel();

  // Initialize Sonar
  this->m_pSonar = new frc::DigitalInput (SONAR_DIO);

  // Initialize Lights
  this->m_pLightsController = new frc::Spark(LIGHTS_PWM);
  this->m_pLights = new Lights(m_pLightsController);

  // Initialize Mechanisms
  this->m_pElevator = new Elevator (this->m_pElevatorMotor,
                                    this->m_pElevatorBottomSwitch,
                                    this->m_pElevatorMiddleSwitch,
                                    this->m_pElevatorTopSwitch);

  this->m_pGripper  = new Gripper (this->m_pGripperRelay);
  this->m_pExtender = new Extender (this->m_pExtenderRelay, this->m_pExtenderLimitSwitch);

  

  frc::Shuffleboard::GetTab("Configuration")
        .AddPersistent("Elevator Hold 3", 0.75)
        .WithWidget("Number Slider");
  

}

void Robot::Autonomous() {
  std::cout << "Sandstorm Started" << std::endl;
  m_robotDrive.SetSafetyEnabled(false);
  while (IsAutonomous() && IsEnabled()) {
    this->Loop();
    frc::Wait(0.005);
  }
}

void Robot::OperatorControl() {
  std::cout << "Sandstorm Ended" << std::endl;
  
  m_robotDrive.SetSafetyEnabled(false);
  while (IsOperatorControl() && IsEnabled()) {
    this->Loop();
    frc::Wait(0.005);
  }
}

void Robot::Loop()
{  
  UpdateSmartDashboard();
  
  HandleJoystickInput();

  if(m_resetGyro) 
  {
    this->m_pGyro->Reset();
    printf ("Gyro Reset!");
  }

  ControlDrive();
}

void Robot::HandleJoystickInput() 
{
    m_autopilot = m_stick.GetRawButton(AUTOPILOT_TOGGLE_BUTTON);

    if (m_stick.GetRawButtonPressed(GYRO_RESET_BUTTON))
    {
      m_resetGyro = true;
    }

    // Elevator Controls
    if (m_operatorStick.GetRawButtonPressed(ELEVATOR_TOP_BUTTON))
    {
      //this->m_pExtender->Move_thread(Extender::Action::EXTEND, false);
      this->m_pElevator->MoveTo_Thread(Elevator::Position::TOP);
    }

    if (m_operatorStick.GetRawButtonPressed(ELEVATOR_LOW_BUTTON))
    {
      this->m_pElevator->MoveTo_Thread(Elevator::Position::BOTTOM);
    }

    if (m_operatorStick.GetRawButtonPressed(ELEVATOR_MID_BUTTON))
    {
      //this->m_pExtender->Move_thread(Extender::Action::EXTEND, false);
      this->m_pElevator->MoveTo_Thread(Elevator::Position::MIDDLE);
    }

    if (m_operatorStick.GetRawButtonPressed(ELEVATOR_STOP_BUTTON))
    {
      this->m_pElevator->Stop();
    }

    // Gripper controls
    if (m_stick.GetRawButtonPressed(GRIPPER_CLOSE_BUTTON))
    {
      this->m_pGripper->Operate_thread(Gripper::Action::CLOSE,false);
    }

    if (m_stick.GetRawButtonPressed(GRIPPER_OPEN_BUTTON))
    {
      this->m_pGripper->Operate_thread(Gripper::Action::OPEN,false);
    }

    // Extender Controls
    if (m_operatorStick.GetRawButtonPressed(EXTENDER_EXTEND_BUTTON))
    {
      this->m_pExtender->Move_thread (Extender::Action::EXTEND, true);
    }
    if (m_operatorStick.GetRawButtonPressed(EXTENDER_RETRACT_BUTTON))
    {
      this->m_pExtender->Move_thread (Extender::Action::RETRACT, false);
    }
}

void Robot::ControlDrive() {
    this->m_pLineSensor->Update();

    Lights::Pattern pattern = Lights::Pattern::TWINKLESGREEN;

    if(m_pLineSensor->FoundLine())
    {
      pattern = Lights::Pattern::STROBEGOLD;
    }
    else
    {
      m_autopilot = false;
    }

    if(m_autopilot)
    {
      m_robotDrive.TankDrive(this->m_pLineSensor->GetLeftAccumulator(), this->m_pLineSensor->GetRightAccumulator());
    }
    else
    {
      m_robotDrive.ArcadeDrive(-m_stick.GetY(), m_stick.GetX());
    }
    
    m_pLights->SetPattern(pattern);
}

void Robot::UpdateSmartDashboard() {
  // Update Line Sensors
  frc::SmartDashboard::PutBoolean("line sensor: left",    this->m_pLeftLineSensor->Get());
  frc::SmartDashboard::PutBoolean("line sensor: center",  this->m_pCenterLineSensor->Get());
  frc::SmartDashboard::PutBoolean("line sensor: right",   this->m_pRightLineSensor->Get());

  // Update Drive Motor info
  frc::SmartDashboard::PutBoolean("sonar", m_pSonar->Get());
  frc::SmartDashboard::PutNumber("accumulator l", m_leftMotor.Get());
  frc::SmartDashboard::PutNumber("accumulator r", -m_rightMotor.Get());
  frc::SmartDashboard::PutData("Left Drive", &m_leftMotor);
  frc::SmartDashboard::PutData("Right Drive", &m_rightMotor);
  frc::SmartDashboard::PutData("Robot Drive", &m_robotDrive);

  // Update Elevator Components
  frc::SmartDashboard::PutData("Elevator Motor", this->m_pElevatorMotor);
  frc::SmartDashboard::PutData("Elevator Limit Switch Bottom", this->m_pElevatorBottomSwitch);
  frc::SmartDashboard::PutData("Elevator Limit Switch Middle", this->m_pElevatorMiddleSwitch);
  frc::SmartDashboard::PutData("Elevator Limit Switch Top", this->m_pElevatorTopSwitch);

  // Update Extender Components 
  frc::SmartDashboard::PutData("Extender Limit Switch", this->m_pExtenderLimitSwitch);
  frc::SmartDashboard::PutData("Extender Relay", this->m_pExtenderRelay);

  // Update Gripper Components
  // frc::SmartDashboard::PutData("Gripper Encoder", this->m_pGripperEncoder);
  frc::SmartDashboard::PutData("Gripper Relay", this->m_pGripperRelay);

  frc::SmartDashboard::PutNumber("DB/Slider 0", frc::SmartDashboard::GetNumber("DB/Slider 1", 0.0));
  frc::SmartDashboard::PutBoolean("DB/Button 0", m_autopilot);

  // Update Misc components
  frc::SmartDashboard::PutData("Gyro", this->m_pGyro);
  frc::SmartDashboard::PutData("Accelerometer", this->m_pAccelerometer);
  // frc::SmartDashboard::PutData("PDP", this->m_pPDP);
  // frc::SmartDashboard::PutNumber("Voltage", this->m_pPDP->GetVoltage());
  frc::SmartDashboard::PutData("Sonar", this->m_pSonar);
  frc::SmartDashboard::PutBoolean("AutoPilot ", m_stick.GetRawButton(AUTOPILOT_TOGGLE_BUTTON));
  

  frc::SmartDashboard::UpdateValues();
}


/**
 * Runs during test mode
 */
void Robot::Test() {}

#ifndef RUNNING_FRC_TESTS
int main() { return frc::StartRobot<Robot>(); }
#endif
