#include "Extender.h"

Extender::Extender ()
{
    // this->m_pEncoder    = NULL;
    this->m_pRelay          = NULL;
    this->m_pLimitSwitch    = NULL;
}

Extender::Extender (frc::Relay *pRelay, frc::DigitalInput *pLimitSwitch):
    Extender::Extender()
{
    this->m_pRelay          = pRelay;
    // this->m_pEncoder        = pEncoder;
    this->m_pLimitSwitch    = pLimitSwitch;
    this->m_pThread         = NULL;
    this->m_threadStatus    = Extender::Status::STOPPED;
    this->m_fStop           = false;
}

void Extender::Move (Extender::Action action, bool fUseEncoder)
{
    frc::Relay::Value direction;
    int duration = 0;
    this->m_fStop = false;

    if (this->m_pThread)
    {
        this->m_threadStatus = Extender::Status::RUNNING;
    }

    switch (action)
    {
        case Action::EXTEND :
            direction = frc::Relay::Value::kReverse;
            duration = EXTENDER_EXTEND_PULSE_LENGTH;
            break;
        case Action::RETRACT :
            direction = frc::Relay::Value::kForward;
            duration = EXTENDER_RETRACT_PULSE_LENGTH;
            break;
        default:
            direction = frc::Relay::Value::kOff;
            break;
    }

    this->m_pRelay->Set(direction);
    if (fUseEncoder)
    {
        while (!this->m_pLimitSwitch->Get() && !this->m_fStop)
        {
            THREAD_SLEEP_MILLISECONDS(5);
        }
    }
    else
    {
        THREAD_SLEEP_MILLISECONDS(duration);
    }
    
    this->m_pRelay->Set(frc::Relay::Value::kOff);

    if (this->m_pThread)
    {
        this->m_threadStatus == Extender::Status::STOPPED;
    }
}

void Extender::Move_thread (Extender::Action action, bool fUseEncoder)
{
    if (this->m_pThread && this->m_threadStatus == Extender::Status::RUNNING)
    {
        this->StopThread();
    }
    this->m_pThread = new std::thread (&Extender::Move, this, action, fUseEncoder);
}

Extender::Status Extender::GetThreadStatus()
{
    return this->m_threadStatus;
}

void Extender::StopThread()
{
    if (this->m_pThread && this->m_threadStatus == Extender::Status::RUNNING)
    {
        this->m_fStop = true;
        this->m_pThread->join();  // Wait for exit
    }
    if (this->m_pThread)
    {
        delete this->m_pThread;
        this->m_pThread = NULL;
    }
    this->m_threadStatus = Extender::Status::STOPPED;
}
void Extender::Extend()
{

}

void Extender::Retract()
{

}