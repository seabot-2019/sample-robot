#pragma once

#include <frc/DigitalInput.h>

class LineSensor {

public:
    LineSensor(
        frc::DigitalInput* leftInput,
        frc::DigitalInput* centerInput,
        frc::DigitalInput* rightInput);

    void Update();
    bool FoundLine();
    double GetLeftAccumulator();
    double GetRightAccumulator();

private:
    double m_leftAccumulator;
    double m_rightAccumulator;
    bool m_left;
    bool m_center;
    bool m_right;
    frc::DigitalInput* m_leftInput;
    frc::DigitalInput* m_centerInput;
    frc::DigitalInput* m_rightInput;
};