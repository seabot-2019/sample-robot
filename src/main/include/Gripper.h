#pragma once

#include <thread>
#include <frc/Relay.h>
#include <frc/Encoder.h>
#include "defines.h"

class Gripper
{
public:
    // Enums
    enum Action {OPEN, CLOSE};
    enum Status {RUNNING, STOPPED};

    // Constructors
    Gripper();
    Gripper(frc::Relay *pRelay);
    //Gripper(frc::Relay *pRelay, frc::Encoder *pEncoder);

    // public operations
    void Operate (Action action, bool fUseEncoder = true);
    void Operate_thread (Action action, bool fUseEncoder = true);
    Status GetThreadStatus();
    void StopThread();
    void Open();
    void Close();

private:
    frc::Relay *m_pRelay;
    frc::Encoder *m_pEncoder;
    std::thread *m_pThread;
    bool m_fStop;
    Status m_ThreadStatus;
    
};