/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <string>
#include <thread>
#include <chrono>

#include <frc/Joystick.h>
#include <frc/PWMVictorSPX.h>
#include <frc/Spark.h>
#include <frc/SampleRobot.h>
#include <frc/drive/DifferentialDrive.h>
#include <frc/smartdashboard/SendableChooser.h>
#include <frc/DigitalInput.h>
#include <frc/AnalogGyro.h>
#include <frc/AnalogAccelerometer.h>
// #include <frc/PowerDistributionPanel.h>
#include <frc/Relay.h>
#include <frc/Encoder.h>
#include <cscore_oo.h>
#include <frc/shuffleboard/Shuffleboard.h>

//#include <frc/DriverStation>

#include "defines.h"

// Mechanisms to include
#include "LineSensor.h"
#include "Lights.h"
#include "Elevator.h"
#include "Gripper.h"
#include "Extender.h"


/**
 * This is a demo program showing the use of the DifferentialDrive class. The
 * SampleRobot class is the base of a robot application that will automatically
 * call your Autonomous and OperatorControl methods at the right time as
 * controlled by the switches on the driver station or the field controls.
 *
 * WARNING: While it may look like a good choice to use for your code if you're
 * inexperienced, don't. Unless you know what you are doing, complex code will
 * be much more difficult under this system. Use TimedRobot or Command-Based
 * instead if you're new.
 */

void blinkLight(int limit);

class Robot : public frc::SampleRobot {
 public:
  Robot();

  void RobotInit() override;
  void Autonomous() override;
  void OperatorControl() override;
  void Test() override;

  void Loop();
  void HandleJoystickInput();
  void ControlDrive();
  void UpdateSmartDashboard();
  

 private:
  // Robot drive system
  frc::PWMVictorSPX m_leftMotor{DRIVE_MOTOR_LEFT_PWM};
  frc::PWMVictorSPX m_rightMotor{DRIVE_MOTOR_RIGHT_PWM};
  frc::DifferentialDrive m_robotDrive{m_leftMotor, m_rightMotor};

  frc::Joystick m_stick {DRIVE_JOYSTICK};
  frc::Joystick m_operatorStick {OPERATOR_JOYSTICK};

  // Elevator
  frc::Spark        *m_pElevatorMotor;
  frc::DigitalInput *m_pElevatorTopSwitch;
  frc::DigitalInput *m_pElevatorMiddleSwitch;
  frc::DigitalInput *m_pElevatorBottomSwitch;

  // Extender
  frc::DigitalInput *m_pExtenderLimitSwitch;
  
  // Encoders
  //frc::Encoder *m_pExtenderEncoder;
  //frc::Encoder *m_pGripperEncoder;

  // Relays
  frc::Relay *m_pGripperRelay;
  frc::Relay *m_pExtenderRelay;

  // Line Sensors
  frc::DigitalInput *m_pLeftLineSensor;
  frc::DigitalInput *m_pCenterLineSensor;
  frc::DigitalInput *m_pRightLineSensor;

  // Misc Items
  // frc::PowerDistributionPanel *m_pPDP; 
  frc::AnalogGyro             *m_pGyro;
  frc::AnalogAccelerometer    *m_pAccelerometer;
  frc::DigitalInput           *m_pSonar;
  frc::Spark                  *m_pLightsController;

  // Robot Mechanisms
  LineSensor  *m_pLineSensor;
  Lights      *m_pLights;
  Elevator    *m_pElevator;
  Gripper     *m_pGripper;
  Extender    *m_pExtender;

  // Camera items
  cs::UsbCamera m_camera;
  cs::VideoSink m_cameraServer;
  
  frc::SendableChooser<std::string> m_chooser;
  const std::string kAutoNameDefault  = "Default";
  const std::string kAutoNameCustom   = "My Auto";

  bool m_autopilot;
  bool m_resetGyro;
  //DriverStation *m_pDriverStation;

};
